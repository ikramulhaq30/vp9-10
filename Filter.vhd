---------------------------------------------------------------------------------- 
-- Filter for Interpolator
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_signed.ALL;

entity Filter is
    Port ( Filter_In_0 : in  STD_LOGIC_VECTOR (7 downto 0);
			  Filter_In_1 : in  STD_LOGIC_VECTOR (7 downto 0);
			  Filter_In_2 : in  STD_LOGIC_VECTOR (7 downto 0);
			  Filter_In_3 : in  STD_LOGIC_VECTOR (7 downto 0);
			  Filter_In_4 : in  STD_LOGIC_VECTOR (7 downto 0);
			  Filter_In_5 : in  STD_LOGIC_VECTOR (7 downto 0);
			  Filter_In_6 : in  STD_LOGIC_VECTOR (7 downto 0);
			  Filter_In_7 : in  STD_LOGIC_VECTOR (7 downto 0);
           Clk : in  STD_LOGIC;
           Reset_n : in  STD_LOGIC;
			  Filter_En : in STD_LOGIC;
           Filter_Out : out  STD_LOGIC_VECTOR (7 downto 0));
end Filter;

architecture Behavioral of Filter is
begin
process(Clk,Reset_n)
	begin
		if (Reset_n = '0') then
			Filter_Out <= (others => '0');
		elsif rising_edge(Clk) then
		    if (Filter_En = '1') then
					Filter_Out <= Filter_In_0;
			 else
			     	Filter_Out <= (others => 'Z');
			end if;
		end if;
end Process;
end Behavioral;

