----------------------------------------------------------------------------------

----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
library work; 
use work.misc.all;

package types_consts is 

CONSTANT width   : INTEGER := 8;
type sample_file is array (filter_taps-1 downto 0) of signed (width-1 downto 0);
type coeff_file is array (filter_taps-1 downto 0) of signed (width-1 downto 0);
--type result_type is array (filter_taps downto 0) of signed (result_width-1  downto 0);
end package;


