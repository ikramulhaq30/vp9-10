--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   16:23:46 12/23/2018
-- Design Name:   
-- Module Name:   F:/Master/Vp9-10/Vp9-10/Sample_One_tb.vhd
-- Project Name:  Vp9-10
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: Sample_One
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values

 
ENTITY Sample_One_tb IS
END Sample_One_tb;
 
ARCHITECTURE behavior OF Sample_One_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT Sample_One
    PORT(
         Data_In : IN  signed(7 downto 0);
         Sel : IN  std_logic_vector(2 downto 0);
         Data_Out : OUT  signed(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal Data_In : signed(7 downto 0) := (others => '0');
   signal Sel : std_logic_vector(2 downto 0) := (others => '0');

 	--Outputs
   signal Data_Out : signed(7 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 

 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: Sample_One PORT MAP (
          Data_In => Data_In,
          Sel => Sel,
          Data_Out => Data_Out
        );


 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 50 ns;	
		Data_In <= "10101011";
		Sel <= "000";
		wait for 50 ns;
		Sel <= "001";
		wait for 50 ns;
		Sel <= "010";
		wait for 50 ns;
		Sel <= "011";
		wait for 50 ns;
		Sel <= "100";
		wait for 50 ns;
		Sel <= "101";
      wait;
   end process;

END;
