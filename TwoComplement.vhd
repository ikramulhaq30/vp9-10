----------------------------------------------------------------------------------

----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity TwoComplement is
    Port ( Data_In : in  Signed (7 downto 0);
           En : in  STD_LOGIC;
           Data_Out : out  Signed (7 downto 0));
end TwoComplement;

architecture Behavioral of TwoComplement is

begin
process (En, Data_In)
begin
   if (En = '1') then
	  Data_out <= not(Data_In) + 1;
	  else
	  Data_out <= Data_In;
	  end if;
end process;
end Behavioral;

