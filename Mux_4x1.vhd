----------------------------------------------------------------------------------

----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_signed.ALL;

entity Mux_4x1 is
    Port ( S0 : in  STD_LOGIC_VECTOR (7 downto 0);
           S1 : in  STD_LOGIC_VECTOR (7 downto 0);
           S2 : in  STD_LOGIC_VECTOR (7 downto 0);
           S3 : in  STD_LOGIC_VECTOR (7 downto 0);
           Sel : in  STD_LOGIC_VECTOR (1 downto 0);
           Data_Out : out  STD_LOGIC_VECTOR (7 downto 0));
end Mux_4x1;

architecture Behavioral of Mux_4x1 is
signal OutReg : STD_LOGIC_VECTOR(7 downto 0) := (others => '0');
begin
Data_Out <= OutReg;
process (S0, S1, S2, S3, Sel)
begin
 case Sel is
				when "00" => 
                OutReg <= S0;  -- 
            when "01" => 
                OutReg <= S1; 
            when "10" => 
                OutReg <= S2;  
            when "11" => 
                OutReg <= S3;
            when others =>
				    OutReg <= (others => 'Z');
  end case;
end process;

end Behavioral;

