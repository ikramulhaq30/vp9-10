----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- Design Name: 
-- Module Name:    Sample_Seven - Behavioral  
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Sample_Seven is
    Port ( Data_In : in  STD_LOGIC_VECTOR (7 downto 0);
           Sel : in  STD_LOGIC_VECTOR (2 downto 0);
           Data_Out : out  STD_LOGIC_VECTOR (7 downto 0));
end Sample_Seven;

architecture Behavioral of Sample_Seven is
component Mux_4x1 
  Port ( S0 : in  STD_LOGIC_VECTOR (7 downto 0);
           S1 : in  STD_LOGIC_VECTOR (7 downto 0);
           S2 : in  STD_LOGIC_VECTOR (7 downto 0);
           S3 : in  STD_LOGIC_VECTOR (7 downto 0);
           Sel : in  STD_LOGIC_VECTOR (1 downto 0);
           Data_Out : out  STD_LOGIC_VECTOR (7 downto 0));
end component;

signal InReg, OutReg : signed(7 downto 0) := (others => '0');
signal S0_Reg, S1_Reg, S2_Reg: signed(7 downto 0) := (others => '0');
signal Mux_level_0_Reg: std_logic_vector(7 downto 0);

begin

Mux_Level_0 : Mux_4x1 
					port map(
					 S0 =>  std_logic_vector(S0_Reg),
					 S1 =>  std_logic_vector(S1_Reg),
					 S2 =>  std_logic_vector(S2_Reg),
					 S3 =>  (others => '-'),
					 Sel =>  Sel(2 downto 1),
					 Data_Out => Mux_level_0_Reg
					);

InReg <= signed(Data_In);
Data_Out <= std_logic_vector(OutReg);
S0_Reg <= shift_left(InReg, 1);
S1_Reg <= shift_left(InReg, 2);
S2_Reg <= shift_left(InReg, 3);

 process (Sel, InReg)
  begin
  case Sel is
				when "000" => 
                OutReg <= shift_left(InReg, 3);  -- 
            when "001" => 
                OutReg <= (others => '0'); 
            when "010" => 
                OutReg <= (signed(Mux_level_0_Reg) + InReg);  
            when "011" => 
                OutReg <=  shift_left((signed(Mux_level_0_Reg) + InReg), 1);
				when "100" => 
                OutReg <=  shift_left((signed(Mux_level_0_Reg) + InReg), 2) - InReg;
            when "101" => 
                OutReg <= shift_left(InReg, 1);    
				when "110" => 
                OutReg <= shift_left(InReg, 2);    					 
            when others =>
                OutReg <= (others => 'Z');
  end case;
end process;

end Behavioral;

