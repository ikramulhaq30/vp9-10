----------------------------------------------------------------------------------
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_signed.ALL;

entity Mux8x1 is
    Port ( S0 : in  STD_LOGIC_VECTOR (7 downto 0);
           S1 : in  STD_LOGIC_VECTOR (7 downto 0);
           S2 : in  STD_LOGIC_VECTOR (7 downto 0);
           S3 : in  STD_LOGIC_VECTOR (7 downto 0);
           S4 : in  STD_LOGIC_VECTOR (7 downto 0);
           S5 : in  STD_LOGIC_VECTOR (7 downto 0);
           S6 : in  STD_LOGIC_VECTOR (7 downto 0);
           S7 : in  STD_LOGIC_VECTOR (7 downto 0);
           Sel : in  STD_LOGIC_VECTOR (2 downto 0);
           Data_Out : out STD_LOGIC_VECTOR (7 downto 0));
end Mux8x1;

architecture Behavioral of Mux8x1 is
signal OutReg : STD_LOGIC_VECTOR(7 downto 0) := (others => '0');
begin
Data_Out <= OutReg;
process (S0, S1, S2, S3, S4, S5, S6, S7, Sel)
begin
 case Sel is
				when "000" => 
                OutReg <= S0;  -- 
            when "001" => 
                OutReg <= S1; 
            when "010" => 
                OutReg <= S2;  
            when "011" => 
                OutReg <= S3;
            when "100" => 
                OutReg <= S4;                 
            when "101" =>
                OutReg <= S5;
				when "110" =>
                OutReg <= S6;
				when "111" =>
                OutReg <= S7;
				when others =>
				    OutReg <= (others => 'Z');
  end case;
end process;


end Behavioral;

