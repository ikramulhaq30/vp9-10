----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    02:12:05 12/23/2018 
-- Design Name: 
-- Module Name:    Mux - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Mux is
    Port ( A : in  STD_LOGIC_VECTOR (7 downto 0);
			  B : in  STD_LOGIC_VECTOR (7 downto 0);
			  Sel : in  STD_LOGIC;
           Data_Out : out  STD_LOGIC_VECTOR (7 downto 0));
end Mux;

architecture Behavioral of Mux is
begin
process(Sel,A,B)
begin
case Sel is 
when '0' => Data_Out<=A;
when '1' => Data_Out<=B;
when others => Data_Out<= (others => 'Z');
end case;
end process;
end Behavioral;
