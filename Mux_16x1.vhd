----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    01:55:16 12/24/2018 
-- Design Name: 
-- Module Name:    Mux_16x1 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Mux_16x1 is
    Port ( S0 : in  STD_LOGIC_VECTOR (7 downto 0);
           S1 : in  STD_LOGIC_VECTOR (7 downto 0);
           S2 : in  STD_LOGIC_VECTOR (7 downto 0);
           S3 : in  STD_LOGIC_VECTOR (7 downto 0);
           S4 : in  STD_LOGIC_VECTOR (7 downto 0);
           S5 : in  STD_LOGIC_VECTOR (7 downto 0);
           S6 : in  STD_LOGIC_VECTOR (7 downto 0);
           S7 : in  STD_LOGIC_VECTOR (7 downto 0);
           S8 : in  STD_LOGIC_VECTOR (7 downto 0);
           S9 : in  STD_LOGIC_VECTOR (7 downto 0);
           S10 : in  STD_LOGIC_VECTOR (7 downto 0);
           S11 : in  STD_LOGIC_VECTOR (7 downto 0);
           S12 : in  STD_LOGIC_VECTOR (7 downto 0);
           S13 : in  STD_LOGIC_VECTOR (7 downto 0);
           S14 : in  STD_LOGIC_VECTOR (7 downto 0);
           S15 : in  STD_LOGIC_VECTOR (7 downto 0);
           Sel : in  STD_LOGIC_VECTOR (3 downto 0);
           Data_Out : out  STD_LOGIC_VECTOR (7 downto 0));
end Mux_16x1;

architecture Behavioral of Mux_16x1 is
signal OutReg : STD_LOGIC_VECTOR(7 downto 0) := (others => '0');
begin
Data_Out <= OutReg;
process (all)
begin
 case Sel is
				when "0000" => 
                OutReg <= S0;  -- 
            when "0001" => 
                OutReg <= S1; 
            when "0010" => 
                OutReg <= S2;  
            when "0011" => 
                OutReg <= S3;
            when "0100" => 
                OutReg <= S4;                 
            when "0101" =>
                OutReg <= S5;
				when "0110" =>
                OutReg <= S6;
				when "0111" =>
                OutReg <= S7;
				when "1000" =>
                OutReg <= S8;
				when "1001" =>
                OutReg <= S9;
				when "1010" =>
                OutReg <= S10;
				when "1011" =>
                OutReg <= S11;
				when others =>
				    OutReg <= (others => '-');
  end case;
end process;
end Behavioral;

