----------------------------------------------------------------------------------
-- 
-- Create Date:    18:29:42 01/05/2019 
-- Design Name: 
-- Module Name:    Sample_Five - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Sample_Five is
    Port ( Data_In : in  STD_LOGIC_VECTOR (7 downto 0);
           Sel : in  STD_LOGIC_VECTOR (3 downto 0);
           Data_Out : out  STD_LOGIC_VECTOR (7 downto 0));
end Sample_Five;

architecture Behavioral of Sample_Five is
component Mux 
     Port ( A : in  STD_LOGIC_VECTOR (7 downto 0);
			  B : in  STD_LOGIC_VECTOR (7 downto 0);
			  Sel : in  STD_LOGIC;
           Data_Out : out  STD_LOGIC_VECTOR (7 downto 0));
end component;

component Mux_4x1 
  Port  (  S0 : in  STD_LOGIC_VECTOR (7 downto 0);
           S1 : in  STD_LOGIC_VECTOR (7 downto 0);
           S2 : in  STD_LOGIC_VECTOR (7 downto 0);
           S3 : in  STD_LOGIC_VECTOR (7 downto 0);
           Sel : in  STD_LOGIC_VECTOR (1 downto 0);
           Data_Out : out  STD_LOGIC_VECTOR (7 downto 0));
end component;

component Mux8x1
   Port ( S0 : in  STD_LOGIC_VECTOR (7 downto 0);
           S1 : in  STD_LOGIC_VECTOR (7 downto 0);
           S2 : in  STD_LOGIC_VECTOR (7 downto 0);
           S3 : in  STD_LOGIC_VECTOR (7 downto 0);
           S4 : in  STD_LOGIC_VECTOR (7 downto 0);
           S5 : in  STD_LOGIC_VECTOR (7 downto 0);
           S6 : in  STD_LOGIC_VECTOR (7 downto 0);
           S7 : in  STD_LOGIC_VECTOR (7 downto 0);
           Sel : in  STD_LOGIC_VECTOR (2 downto 0);
           Data_Out : out STD_LOGIC_VECTOR (7 downto 0));
end component;

component AddSub
 Port ( A : in  signed (7 downto 0);
        B : in  signed (7 downto 0);
        Add_Sub : in  STD_LOGIC;
        Data_Out : out  signed (7 downto 0));
end component;

signal InReg, OutReg : signed(7 downto 0) := (others => '0');
signal Add_Sub_Reg_Level_0, Add_Sub_Reg_Level_1 : signed(7 downto 0) := (others => '0');

signal S0_Level_0_Reg, S1_Level_0_Reg, S2_Level_0_Reg: signed(7 downto 0) := (others => '0');
signal S0_Level_2_Reg, S1_Level_2_Reg, S2_Level_2_Reg, S3_Level_2_Reg, S4_Level_2_Reg, S5_Level_2_Reg, S6_Level_2_Reg : signed(7 downto 0) := (others => '0');
signal A_Level_1_Reg, B_Level_1_Reg: signed(7 downto 0) := (others => '0');
signal Add_Sub_Level_0_Reg: signed(7 downto 0) := (others => '0');
signal Mux_4x1_Level_0_Reg, Mux_2x1_Level_1_Reg, Mux_8x1_Level_2_Reg : std_logic_vector(7 downto 0);

begin
	Mux_4x1_Level_0 : Mux_4x1 
               port map(
					S0 => std_logic_vector(S0_Level_0_Reg),
					S1 => std_logic_vector(S1_Level_0_Reg),
					S2 => std_logic_vector(S2_Level_0_Reg),
					S3 => (others => '-'),
					Sel => Sel(3 downto 2),
					Data_Out => Mux_4x1_Level_0_Reg
					);
									
	Mux_Level_1 : Mux 
					port map(
					 A =>  std_logic_vector(A_Level_1_Reg),
					 B =>  std_logic_vector(B_Level_1_Reg),
					 Sel =>  Sel(3),
					 Data_Out => Mux_2x1_Level_1_Reg
					);
						
	Add_Sub_Level_0 : AddSub 
                   port map(
						 A => Add_Sub_Level_0_Reg,
						 B => InReg,
						 Add_Sub => Sel(3),
						 Data_Out => Add_Sub_Reg_Level_0
						 );
	
	Mux_8x1_Level_2 : Mux8x1 
               port map(
					S0 => std_logic_vector(S0_Level_2_Reg),
					S1 => std_logic_vector(S1_Level_2_Reg),
					S2 => std_logic_vector(S2_Level_2_Reg),
					S3 => std_logic_vector(S3_Level_2_Reg),
					S4 => std_logic_vector(S4_Level_2_Reg),
					S5 => std_logic_vector(S5_Level_2_Reg),
					S6 => std_logic_vector(S6_Level_2_Reg),
					S7 => (others => '-'),
					Sel => Sel(3 downto 1),
					Data_Out => Mux_8x1_Level_2_Reg
					);
					
	Add_Sub_Level_1 : AddSub 
                   port map(
						 A => signed(Mux_2x1_Level_1_Reg),
						 B => signed(Mux_8x1_Level_2_Reg),
						 Add_Sub => Sel(3),
						 Data_Out => Add_Sub_Reg_Level_1
						 );			
						 
	InReg <= signed(Data_In);
	Data_Out <= std_logic_vector(OutReg);
	
	S0_Level_0_Reg <= shift_left(InReg, 1);
	S1_Level_0_Reg <= shift_left(InReg, 2);
	S2_Level_0_Reg <= shift_left(InReg, 3);
	
	A_Level_1_Reg <= InReg;
	B_Level_1_Reg <= signed(Mux_4x1_Level_0_Reg) + InReg;

   Add_Sub_Level_0_Reg <= shift_left(B_Level_1_Reg, 2);

	S0_Level_2_Reg <= shift_left(B_Level_1_Reg, 4);
	S1_Level_2_Reg <= shift_left(B_Level_1_Reg, 3);
	S2_Level_2_Reg <= Add_Sub_Level_0_Reg;
	S3_Level_2_Reg <= shift_left(InReg, 4);
	S4_Level_2_Reg <= shift_left(InReg, 3);
	S5_Level_2_Reg <= shift_left(InReg, 6);
	S6_Level_2_Reg <= shift_left(InReg, 5);

  process (Sel, InReg)
  begin
  case Sel is
				when "0000" => 
                OutReg <= shift_left(B_Level_1_Reg, 3);  
            when "0001" => 
                OutReg <= shift_left(B_Level_1_Reg, 4); 
            when "0010" => 
                OutReg <= Add_Sub_Reg_Level_1;  
            when "0011" => 
                OutReg <= shift_left(Add_Sub_Reg_Level_1, 1);
            when "0100" => 
                OutReg <= shift_left(Add_Sub_Reg_Level_1, 2);   
            when "0101" => 
                OutReg <= shift_left(Add_Sub_Reg_Level_1, 3); 
            when "0110" => 
                OutReg <= signed(Mux_8x1_Level_2_Reg); 	
            when "0111" => 
                OutReg <= shift_left(InReg, 5); 
				when "1000" => 
                OutReg <= shift_left(InReg, 6); 			
				when "1001" => 
                OutReg <= shift_left(InReg, 3); 
				when "1010" => 
                OutReg <= shift_left(InReg, 4); 	
            when others =>
                OutReg <= (others => 'Z');
  end case;
end process;

end Behavioral;

