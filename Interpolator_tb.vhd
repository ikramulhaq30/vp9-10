-- TestBench Template 

  LIBRARY ieee;
  USE ieee.std_logic_1164.ALL;
	use ieee.numeric_std.all;
	use std.textio.all;

  ENTITY Interpolator_test IS
  END Interpolator_test;

  ARCHITECTURE behavior OF Interpolator_test IS 

  -- Component Declaration
          COMPONENT Interpolator
           Port ( 	
			   Sample_in_0 : in  STD_LOGIC_VECTOR (7 downto 0);
				Sample_in_1 : in  STD_LOGIC_VECTOR (7 downto 0);
				Sample_in_2 : in  STD_LOGIC_VECTOR (7 downto 0);
				Sample_in_3 : in  STD_LOGIC_VECTOR (7 downto 0);
				Sample_in_4 : in  STD_LOGIC_VECTOR (7 downto 0);
				Sample_in_5 : in  STD_LOGIC_VECTOR (7 downto 0);
				Sample_in_6 : in  STD_LOGIC_VECTOR (7 downto 0);
				Sample_in_7 : in  STD_LOGIC_VECTOR (7 downto 0);
				Sample_in_8 : in  STD_LOGIC_VECTOR (7 downto 0);
				Sample_in_9 : in  STD_LOGIC_VECTOR (7 downto 0);
				Sample_in_10 : in  STD_LOGIC_VECTOR (7 downto 0);
				Sample_in_11 : in  STD_LOGIC_VECTOR (7 downto 0);	
				Sample_in_12 : in  STD_LOGIC_VECTOR (7 downto 0);
				Sample_in_13 : in  STD_LOGIC_VECTOR (7 downto 0);
				Sample_in_14 : in  STD_LOGIC_VECTOR (7 downto 0);
            Clk : in  STD_LOGIC;
            Reset_n : in  STD_LOGIC;
            Interpolator_out_0 : out  STD_LOGIC_VECTOR (7 downto 0);
			   Interpolator_out_1 : out  STD_LOGIC_VECTOR (7 downto 0);
			   Interpolator_out_2 : out  STD_LOGIC_VECTOR (7 downto 0);
			   Interpolator_out_3 : out  STD_LOGIC_VECTOR (7 downto 0);
			   Interpolator_out_4 : out  STD_LOGIC_VECTOR (7 downto 0);
			   Interpolator_out_5 : out  STD_LOGIC_VECTOR (7 downto 0);
			   Interpolator_out_6 : out  STD_LOGIC_VECTOR (7 downto 0);
			   Interpolator_out_7 : out  STD_LOGIC_VECTOR (7 downto 0));
          END COMPONENT;

        SIGNAL Clk         : STD_LOGIC := '0';
		  SIGNAL Reset_n       : STD_LOGIC := '0';
		  SIGNAL Sample_in_0, Sample_in_1, Sample_in_2, Sample_in_3, Sample_in_4, Sample_in_5, Sample_in_6, Sample_in_7 : STD_LOGIC_VECTOR(7 downto 0) := (others => '0');
		  SIGNAL Sample_in_8, Sample_in_9, Sample_in_10, Sample_in_11, Sample_in_12, Sample_in_13, Sample_in_14 : STD_LOGIC_VECTOR(7 downto 0) := (others => '0');
		  SIGNAL Interpolator_out_0, Interpolator_out_1, Interpolator_out_2, Interpolator_out_3:  std_logic_vector(7 downto 0);
        SIGNAL Interpolator_out_4, Interpolator_out_5, Interpolator_out_6, Interpolator_out_7:  std_logic_vector(7 downto 0);  
 
		-- Clock period definitions
		constant clk_period : time := 10 ns;
  BEGIN

  -- Component Instantiation
          uut: Interpolator
    			 PORT MAP(
            Sample_in_0 => Sample_in_0,
				Sample_in_1 => Sample_in_1,
				Sample_in_2 => Sample_in_2,
				Sample_in_3 => Sample_in_3,
				Sample_in_4 => Sample_in_4,
				Sample_in_5 => Sample_in_5,
				Sample_in_6 => Sample_in_6,
				Sample_in_7 => Sample_in_7,
				Sample_in_8 => Sample_in_8,
				Sample_in_9 => Sample_in_9,
				Sample_in_10 => Sample_in_10, 
				Sample_in_11 => Sample_in_11,
				Sample_in_12 => Sample_in_12,
				Sample_in_13 => Sample_in_13,
				Sample_in_14 => Sample_in_14,
            Clk => Clk,
            Reset_n => Reset_n,
            Interpolator_out_0 => Interpolator_out_0, 
			   Interpolator_out_1 => Interpolator_out_1,
			   Interpolator_out_2 => Interpolator_out_2,
			   Interpolator_out_3 => Interpolator_out_3,
			   Interpolator_out_4 => Interpolator_out_4,
			   Interpolator_out_5 => Interpolator_out_5,
			   Interpolator_out_6 => Interpolator_out_6,
			   Interpolator_out_7 => Interpolator_out_7 
          );


   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
	
      -- Stimulus process
   stim_proc: process
	
	file INPUT_FILE : text open read_mode is "input.txt";
	file OUTPUT_FILE : text open write_mode is "output_int.txt";
	
	file INPUT_FILE_DEC : text open write_mode is "input-dec_int.txt";
	file OUTPUT_FILE_DEC : text open write_mode is "output-dec_int.txt";

   variable input_line : LINE;
	variable output_line: LINE;
	variable input_dec_line : LINE;
	variable output_dec_line: LINE;
	
	variable str : bit_vector(15*8-1 downto 0) ;
	variable b : boolean;
	
	begin		
   
      wait for 50 ns;	
		Reset_n <= '1';
		
		for i in 1 to 15 loop -- read 15 samples
			
			wait until clk'event and clk = '1'; -- wait until the positive edge of the clk
		
			readline (INPUT_FILE,input_line); -- read one line from the input file
			read(input_line,str);				 -- parse that line for 8 bits vector
			Sample_in_0 <= to_stdlogicvector(str(119 downto 112));	 -- convert to std_logic_vector type
			Sample_in_1 <= to_stdlogicvector(str(111 downto 104));	 -- convert to std_logic_vector type
			Sample_in_2 <= to_stdlogicvector(str(103 downto 96));	 -- convert to std_logic_vector type
			Sample_in_3 <= to_stdlogicvector(str(95 downto 88));	 -- convert to std_logic_vector type
		   Sample_in_4 <= to_stdlogicvector(str(87 downto 80));	 -- convert to std_logic_vector type
			Sample_in_5 <= to_stdlogicvector(str(79 downto 72));	 -- convert to std_logic_vector type
			Sample_in_6 <= to_stdlogicvector(str(71 downto 64));	 -- convert to std_logic_vector type
			Sample_in_7 <= to_stdlogicvector(str(63 downto 56));	 -- convert to std_logic_vector type
			Sample_in_8 <= to_stdlogicvector(str(55 downto 48));	 -- convert to std_logic_vector type
			Sample_in_9 <= to_stdlogicvector(str(47 downto 40));	 -- convert to std_logic_vector type
			Sample_in_10 <= to_stdlogicvector(str(39 downto 32));	 -- convert to std_logic_vector type
			Sample_in_11 <= to_stdlogicvector(str(31 downto 24));	 -- convert to std_logic_vector type
			Sample_in_12 <= to_stdlogicvector(str(23 downto 16));	 -- convert to std_logic_vector type
			Sample_in_13 <= to_stdlogicvector(str(15 downto 8));	 -- convert to std_logic_vector type
			Sample_in_14 <= to_stdlogicvector(str(7 downto 0));	 -- convert to std_logic_vector type
			
			write(output_line,to_bitvector(Interpolator_out_0)); -- write the result to the output line
			writeline(output_file,output_line);		-- write the line to the output file
			
			write(output_dec_line,to_integer(unsigned(Interpolator_out_0)));
			writeline(output_file_dec,output_dec_line);
			
			write(input_dec_line,to_integer(unsigned(Sample_in_0)));
			writeline(input_file_dec,input_dec_line);

		end loop;	
   end process;

  END;
  

