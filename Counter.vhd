------------------------------------------------------
-- File Name : counter.vhd
------------------------------------------------------
Library IEEE;
use IEEE.std_logic_1164.ALL;
use IEEE.std_logic_signed.ALL;

entity Counter is
	port (Clk, Reset_n : in std_logic;
			Filter_En_H : out std_logic;
			Filter_En_V : out std_logic
	);
end;

architecture Arch_counter of Counter is

begin
-----------------------------------------------------
process (Clk, Reset_n)
variable COUNT : std_logic_vector(3 downto 0);
begin
	if Reset_n = '0' then
		COUNT := (others=>'0');
		Filter_En_H <= '0';
		Filter_En_V <= '0';
	elsif (rising_edge(Clk)) then
			 COUNT := COUNT+1;
		if (COUNT = 15) then 
			Filter_En_V <= '0';
			COUNT := (others => '0');
			elsif (COUNT = 7) then 
			Filter_En_V <= '1';
			else
			Filter_En_H <= '1';
		end if;
	end if;
end process;
-----------------------------------------------------
end; -- Arch_counter


