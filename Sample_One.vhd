----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:53:09 12/23/2018 
-- Design Name: 
-- Module Name:    Sample_One - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity Sample_One is
    Port ( Data_In : in  signed (7 downto 0);
           Sel : in  std_logic_vector (2 downto 0);
           Data_Out : out signed (7 downto 0));
end Sample_One;

architecture Behavioral of Sample_One is
signal InReg,OutReg : signed(7 downto 0) := (others => '0');
begin
InReg <= Data_In;
Data_Out <= OutReg;
  process (Sel, InReg)
  begin
  case Sel is
				when "000" => 
                OutReg <= InReg;  -- 
            when "001" => 
                OutReg <= (shift_left(InReg, 1) + InReg); 
            when "010" => 
                OutReg <= shift_left(InReg, 1);  
            when "011" => 
                OutReg <= (others => '0');
            when "100" => 
                OutReg <= shift_left(InReg, 2);                 
            when others =>
                OutReg <= (others => '0');
  end case;
  end process;
end Behavioral;

