----------------------------------------------------------------------------------

----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.std_logic_signed.ALL;


entity Sample_Two is
    Port ( Data_In : in  STD_LOGIC_VECTOR (7 downto 0);
           Sel : in  STD_LOGIC_VECTOR (2 downto 0);
           Data_Out : out  STD_LOGIC_VECTOR (7 downto 0));
end Sample_Two;

architecture Behavioral of Sample_Two is
component Mux_4x1 
  Port ( S0 : in  STD_LOGIC_VECTOR (7 downto 0);
           S1 : in  STD_LOGIC_VECTOR (7 downto 0);
           S2 : in  STD_LOGIC_VECTOR (7 downto 0);
           S3 : in  STD_LOGIC_VECTOR (7 downto 0);
           Sel : in  STD_LOGIC_VECTOR (1 downto 0);
           Data_Out : out  STD_LOGIC_VECTOR (7 downto 0));
end component;

component AddSub
 Port ( A : in  signed (7 downto 0);
        B : in  signed (7 downto 0);
        Add_Sub : in  STD_LOGIC;
        Data_Out : out  signed (7 downto 0));
end component;

signal InReg, OutReg : signed(7 downto 0) := (others => '0');
signal Mux_Reg, Add_Sub_Reg, S0_Reg, S1_Reg, S2_Reg, S3_Reg: signed(7 downto 0) := (others => '0');
signal Mux_Reg_Std : std_logic_vector(7 downto 0);

begin

Mux_Levl_0 : Mux_4x1 
					port map(
					 S0 =>  std_logic_vector(S0_Reg),
					 S1 =>  std_logic_vector(S1_Reg),
					 S2 =>  std_logic_vector(S2_Reg),
					 S3 =>  std_logic_vector(S3_Reg),
					 Sel =>  Sel(1 downto 0),
					 Data_Out => Mux_Reg_Std
					);
					
Add_Sub_Level_0 : AddSub 
                   port map(
						 A => InReg,
						 B => signed(Mux_Reg_Std),
						 Add_Sub => Sel(2),
						 Data_Out => Add_Sub_Reg
						 );			
						 
InReg <= signed(Data_In);
Data_Out <= std_logic_vector(OutReg);
S0_Reg <= InReg + shift_left(InReg, 2);
S1_Reg <= shift_left(InReg,1);
S2_Reg <=  shift_left(InReg,3);
S3_Reg <= shift_left(InReg,2);

  process (Sel, InReg)
  begin
  case Sel is
				when "000" => 
                OutReg <= InReg;  -- 
            when "001" => 
                OutReg <= Add_Sub_Reg; 
            when "010" => 
                OutReg <= shift_left(Add_Sub_Reg, 1);  
            when "011" => 
                OutReg <= (others => '0');
            when "100" => 
                OutReg <= shift_left(InReg, 2);                 
            when others =>
                OutReg <= (others => 'Z');
  end case;
end process;

end Behavioral;

