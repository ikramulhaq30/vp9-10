----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    01:33:40 12/23/2018 
-- Design Name: 
-- Module Name:    Interpolator - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Interpolator is
    Port ( 	Sample_in_0 : in  STD_LOGIC_VECTOR (7 downto 0);
				Sample_in_1 : in  STD_LOGIC_VECTOR (7 downto 0);
				Sample_in_2 : in  STD_LOGIC_VECTOR (7 downto 0);
				Sample_in_3 : in  STD_LOGIC_VECTOR (7 downto 0);
				Sample_in_4 : in  STD_LOGIC_VECTOR (7 downto 0);
				Sample_in_5 : in  STD_LOGIC_VECTOR (7 downto 0);
				Sample_in_6 : in  STD_LOGIC_VECTOR (7 downto 0);
				Sample_in_7 : in  STD_LOGIC_VECTOR (7 downto 0);
				Sample_in_8 : in  STD_LOGIC_VECTOR (7 downto 0);
				Sample_in_9 : in  STD_LOGIC_VECTOR (7 downto 0);
				Sample_in_10 : in  STD_LOGIC_VECTOR (7 downto 0);
				Sample_in_11 : in  STD_LOGIC_VECTOR (7 downto 0);	
				Sample_in_12 : in  STD_LOGIC_VECTOR (7 downto 0);
				Sample_in_13 : in  STD_LOGIC_VECTOR (7 downto 0);
				Sample_in_14 : in  STD_LOGIC_VECTOR (7 downto 0);
            Clk : in  STD_LOGIC;
            Reset_n : in  STD_LOGIC;
            Interpolator_out_0 : out  STD_LOGIC_VECTOR (7 downto 0);
			   Interpolator_out_1 : out  STD_LOGIC_VECTOR (7 downto 0);
			   Interpolator_out_2 : out  STD_LOGIC_VECTOR (7 downto 0);
			   Interpolator_out_3 : out  STD_LOGIC_VECTOR (7 downto 0);
			   Interpolator_out_4 : out  STD_LOGIC_VECTOR (7 downto 0);
			   Interpolator_out_5 : out  STD_LOGIC_VECTOR (7 downto 0);
			   Interpolator_out_6 : out  STD_LOGIC_VECTOR (7 downto 0);
			   Interpolator_out_7 : out  STD_LOGIC_VECTOR (7 downto 0));
end Interpolator;

architecture Behavioral of Interpolator is

   component Counter 
	   port (Clk, Reset_n : in std_logic;
			Filter_En_H : out std_logic;
			Filter_En_V : out std_logic);
	end Component;
	
	component Filter
		Port ( Filter_In_0 : in  STD_LOGIC_VECTOR (7 downto 0);
				 Filter_In_1 : in  STD_LOGIC_VECTOR (7 downto 0);
				 Filter_In_2 : in  STD_LOGIC_VECTOR (7 downto 0);
				 Filter_In_3 : in  STD_LOGIC_VECTOR (7 downto 0);
				 Filter_In_4 : in  STD_LOGIC_VECTOR (7 downto 0);
				 Filter_In_5 : in  STD_LOGIC_VECTOR (7 downto 0);
				 Filter_In_6 : in  STD_LOGIC_VECTOR (7 downto 0);
				 Filter_In_7 : in  STD_LOGIC_VECTOR (7 downto 0);
				 Clk : in  STD_LOGIC;
				 Reset_n : in  STD_LOGIC;
				 Filter_En : in STD_LOGIC;
				 Filter_Out : out  STD_LOGIC_VECTOR (7 downto 0));   
	end component;
	
	component Shifter
			Port ( 
				Data_In : in  STD_LOGIC_VECTOR (7 downto 0);
				Clk : in  STD_LOGIC;
				Reset_n : in  STD_LOGIC;
				Data_Out : out STD_LOGIC_VECTOR (7 downto 0));
	end component;
	
	-- signal Data_Gating_Out_0, Data_Gating_Out_1, Data_Gating_Out_2, Data_Gating_Out_3, Data_Gating_Out_4, Data_Gating_Out_5, Data_Gating_Out_6, Data_Gating_Out_7	:std_logic_vector(7 downto 0);
	-- signal H_Mux_Out_0, H_Mux_Out_1, H_Mux_Out_2, H_Mux_Out_3, H_Mux_Out_4, H_Mux_Out_5, H_Mux_Out_6, H_Mux_Out_7	:std_logic_vector(7 downto 0);
  signal H_Filter_Out_0, H_Filter_Out_1, H_Filter_Out_2, H_Filter_Out_3, H_Filter_Out_4, H_Filter_Out_5, H_Filter_Out_6, H_Filter_Out_7 : std_logic_vector(7 downto 0);
  signal Shifter_Out_H0_0, Shifter_Out_H0_1, Shifter_Out_H0_2, Shifter_Out_H0_3, Shifter_Out_H0_4, Shifter_Out_H0_5, Shifter_Out_H0_6, Shifter_Out_H0_7 : std_logic_vector(7 downto 0);
  signal Shifter_Out_H1_0, Shifter_Out_H1_1, Shifter_Out_H1_2, Shifter_Out_H1_3, Shifter_Out_H1_4, Shifter_Out_H1_5, Shifter_Out_H1_6, Shifter_Out_H1_7 : std_logic_vector(7 downto 0);
  signal Shifter_Out_H2_0, Shifter_Out_H2_1, Shifter_Out_H2_2, Shifter_Out_H2_3, Shifter_Out_H2_4, Shifter_Out_H2_5, Shifter_Out_H2_6, Shifter_Out_H2_7 : std_logic_vector(7 downto 0);
  signal Shifter_Out_H3_0, Shifter_Out_H3_1, Shifter_Out_H3_2, Shifter_Out_H3_3, Shifter_Out_H3_4, Shifter_Out_H3_5, Shifter_Out_H3_6, Shifter_Out_H3_7 : std_logic_vector(7 downto 0);
  signal Shifter_Out_H4_0, Shifter_Out_H4_1, Shifter_Out_H4_2, Shifter_Out_H4_3, Shifter_Out_H4_4, Shifter_Out_H4_5, Shifter_Out_H4_6, Shifter_Out_H4_7 : std_logic_vector(7 downto 0);
  signal Shifter_Out_H5_0, Shifter_Out_H5_1, Shifter_Out_H5_2, Shifter_Out_H5_3, Shifter_Out_H5_4, Shifter_Out_H5_5, Shifter_Out_H5_6, Shifter_Out_H5_7 : std_logic_vector(7 downto 0);
  signal Shifter_Out_H6_0, Shifter_Out_H6_1, Shifter_Out_H6_2, Shifter_Out_H6_3, Shifter_Out_H6_4, Shifter_Out_H6_5, Shifter_Out_H6_6, Shifter_Out_H6_7 : std_logic_vector(7 downto 0);
  signal Shifter_Out_H7_0, Shifter_Out_H7_1, Shifter_Out_H7_2, Shifter_Out_H7_3, Shifter_Out_H7_4, Shifter_Out_H7_5, Shifter_Out_H7_6, Shifter_Out_H7_7 : std_logic_vector(7 downto 0);
  signal Filter_H_Enable, Filter_V_Enable : std_logic;
begin


Counter_Fsm : Counter 
				  port map(
				  Clk => Clk,
				  Reset_n => Reset_n,
				  Filter_En_H => Filter_H_Enable,
				  Filter_En_V => Filter_V_Enable
				  );
				  
H_Filter_0: Filter
				port map(
					Filter_In_0 => Sample_in_0,
					Filter_In_1 => Sample_in_1,
					Filter_In_2 => Sample_in_2,
					Filter_In_3 => Sample_in_3,
					Filter_In_4 => Sample_in_4,
					Filter_In_5 => Sample_in_5,
					Filter_In_6 => Sample_in_6,
					Filter_In_7 => Sample_in_7,
					Clk => Clk,
					Reset_n => Reset_n,
					Filter_En => Filter_H_Enable,
					Filter_Out => H_Filter_Out_0);

H_Filter_1: Filter
				port map(
					Filter_In_0 => Sample_in_1,
					Filter_In_1 => Sample_in_2,
					Filter_In_2 => Sample_in_3,
					Filter_In_3 => Sample_in_4,
					Filter_In_4 => Sample_in_5,
					Filter_In_5 => Sample_in_6,
					Filter_In_6 => Sample_in_7,
					Filter_In_7 => Sample_in_8,
					Clk => Clk,
					Reset_n => Reset_n,
					Filter_En => Filter_H_Enable,
					Filter_Out => H_Filter_Out_1);	

H_Filter_2: Filter
				port map(
					Filter_In_0 => Sample_in_2,
					Filter_In_1 => Sample_in_3,
					Filter_In_2 => Sample_in_4,
					Filter_In_3 => Sample_in_5,
					Filter_In_4 => Sample_in_6,
					Filter_In_5 => Sample_in_7,
					Filter_In_6 => Sample_in_8,
					Filter_In_7 => Sample_in_9,
					Clk => Clk,
					Reset_n => Reset_n,
					Filter_En => Filter_H_Enable,
					Filter_Out => H_Filter_Out_2);					
					
H_Filter_3: Filter
				port map(
					Filter_In_0 => Sample_in_3,
					Filter_In_1 => Sample_in_4,
					Filter_In_2 => Sample_in_5,
					Filter_In_3 => Sample_in_6,
					Filter_In_4 => Sample_in_7,
					Filter_In_5 => Sample_in_8,
					Filter_In_6 => Sample_in_9,
					Filter_In_7 => Sample_in_10,
					Clk => Clk,
					Reset_n => Reset_n,
					Filter_En => Filter_H_Enable,
					Filter_Out => H_Filter_Out_3);	
					
H_Filter_4: Filter
				port map(
					Filter_In_0 => Sample_in_4,
					Filter_In_1 => Sample_in_5,
					Filter_In_2 => Sample_in_6,
					Filter_In_3 => Sample_in_7,
					Filter_In_4 => Sample_in_8,
					Filter_In_5 => Sample_in_9,
					Filter_In_6 => Sample_in_10,
					Filter_In_7 => Sample_in_11,
					Clk => Clk,
					Reset_n => Reset_n,
					Filter_En => Filter_H_Enable,
					Filter_Out => H_Filter_Out_4);	
					
H_Filter_5: Filter
				port map(
					Filter_In_0 => Sample_in_5,
					Filter_In_1 => Sample_in_6,
					Filter_In_2 => Sample_in_7,
					Filter_In_3 => Sample_in_8,
					Filter_In_4 => Sample_in_9,
					Filter_In_5 => Sample_in_10,
					Filter_In_6 => Sample_in_11,
					Filter_In_7 => Sample_in_12,
					Clk => Clk,
					Reset_n => Reset_n,
					Filter_En => Filter_H_Enable,
					Filter_Out => H_Filter_Out_5);	

H_Filter_6: Filter
				port map(
					Filter_In_0 => Sample_in_6,
					Filter_In_1 => Sample_in_7,
					Filter_In_2 => Sample_in_8,
					Filter_In_3 => Sample_in_9,
					Filter_In_4 => Sample_in_10,
					Filter_In_5 => Sample_in_11,
					Filter_In_6 => Sample_in_12,
					Filter_In_7 => Sample_in_13,
					Clk => Clk,
					Reset_n => Reset_n,
					Filter_En => Filter_H_Enable,
					Filter_Out => H_Filter_Out_6);	

H_Filter_7: Filter
				port map(
					Filter_In_0 => Sample_in_7,
					Filter_In_1 => Sample_in_8,
					Filter_In_2 => Sample_in_9,
					Filter_In_3 => Sample_in_10,
					Filter_In_4 => Sample_in_11,
					Filter_In_5 => Sample_in_12,
					Filter_In_6 => Sample_in_13,
					Filter_In_7 => Sample_in_14,
					Clk => Clk,
					Reset_n => Reset_n,
					Filter_En => Filter_H_Enable,
					Filter_Out => H_Filter_Out_7);										
					
Shifter_H0_0: Shifter 
             port map(
					Data_In => H_Filter_Out_0,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H0_0);

Shifter_H0_1: Shifter 
             port map(
					Data_In => Shifter_Out_H0_0,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H0_1);
					
Shifter_H0_2: Shifter 
             port map(
					Data_In => Shifter_Out_H0_1,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H0_2);
					
Shifter_H0_3: Shifter 
             port map(
					Data_In => Shifter_Out_H0_2,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H0_3);
					
Shifter_H0_4: Shifter 
             port map(
					Data_In => Shifter_Out_H0_3,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H0_4);
					
Shifter_H0_5: Shifter 
             port map(
					Data_In => Shifter_Out_H0_4,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H0_5);
					
Shifter_H0_6: Shifter 
             port map(
					Data_In => Shifter_Out_H0_5,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H0_6);
					
Shifter_H0_7: Shifter 
             port map(
					Data_In => Shifter_Out_H0_6,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H0_7);
					
Shifter_H1_0: Shifter 
             port map(
					Data_In => H_Filter_Out_1,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H1_0);

Shifter_H1_1: Shifter 
             port map(
					Data_In => Shifter_Out_H1_0,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H1_1);
					
Shifter_H1_2: Shifter 
             port map(
					Data_In => Shifter_Out_H1_1,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H1_2);
					
Shifter_H1_3: Shifter 
             port map(
					Data_In => Shifter_Out_H1_2,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H1_3);
					
Shifter_H1_4: Shifter 
             port map(
					Data_In => Shifter_Out_H1_3,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H1_4);
					
Shifter_H1_5: Shifter 
             port map(
					Data_In => Shifter_Out_H1_4,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H1_5);
					
Shifter_H1_6: Shifter 
             port map(
					Data_In => Shifter_Out_H1_5,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H1_6);
					
Shifter_H1_7: Shifter 
             port map(
					Data_In => Shifter_Out_H1_6,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H1_7);

Shifter_H2_0: Shifter 
             port map(
					Data_In => H_Filter_Out_2,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H2_0);

Shifter_H2_1: Shifter 
             port map(
					Data_In => Shifter_Out_H2_0,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H2_1);
					
Shifter_H2_2: Shifter 
             port map(
					Data_In => Shifter_Out_H2_1,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H2_2);
					
Shifter_H2_3: Shifter 
             port map(
					Data_In => Shifter_Out_H2_2,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H2_3);
					
Shifter_H2_4: Shifter 
             port map(
					Data_In => Shifter_Out_H2_3,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H2_4);
					
Shifter_H2_5: Shifter 
             port map(
					Data_In => Shifter_Out_H2_4,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H2_5);
					
Shifter_H2_6: Shifter 
             port map(
					Data_In => Shifter_Out_H2_5,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H2_6);
					
Shifter_H2_7: Shifter 
             port map(
					Data_In => Shifter_Out_H2_6,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H2_7);
					
Shifter_H3_0: Shifter 
             port map(
					Data_In => H_Filter_Out_3,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H3_0);

Shifter_H3_1: Shifter 
             port map(
					Data_In => Shifter_Out_H3_0,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H3_1);
					
Shifter_H3_2: Shifter 
             port map(
					Data_In => Shifter_Out_H3_1,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H3_2);
					
Shifter_H3_3: Shifter 
             port map(
					Data_In => Shifter_Out_H3_2,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H3_3);
					
Shifter_H3_4: Shifter 
             port map(
					Data_In => Shifter_Out_H3_3,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H3_4);
					
Shifter_H3_5: Shifter 
             port map(
					Data_In => Shifter_Out_H3_4,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H3_5);
					
Shifter_H3_6: Shifter 
             port map(
					Data_In => Shifter_Out_H3_5,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H3_6);
					
Shifter_H3_7: Shifter 
             port map(
					Data_In => Shifter_Out_H3_6,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H3_7);
					
Shifter_H4_0: Shifter 
             port map(
					Data_In => H_Filter_Out_4,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H4_0);

Shifter_H4_1: Shifter 
             port map(
					Data_In => Shifter_Out_H4_0,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H4_1);
					
Shifter_H4_2: Shifter 
             port map(
					Data_In => Shifter_Out_H4_1,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H4_2);
					
Shifter_H4_3: Shifter 
             port map(
					Data_In => Shifter_Out_H4_2,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H4_3);
					
Shifter_H4_4: Shifter 
             port map(
					Data_In => Shifter_Out_H4_3,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H4_4);
					
Shifter_H4_5: Shifter 
             port map(
					Data_In => Shifter_Out_H4_4,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H4_5);
					
Shifter_H4_6: Shifter 
             port map(
					Data_In => Shifter_Out_H4_5,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H4_6);
					
Shifter_H4_7: Shifter 
             port map(
					Data_In => Shifter_Out_H4_6,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H4_7);
					
Shifter_H5_0: Shifter 
             port map(
					Data_In => H_Filter_Out_5,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H5_0);

Shifter_H5_1: Shifter 
             port map(
					Data_In => Shifter_Out_H5_0,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H5_1);
					
Shifter_H5_2: Shifter 
             port map(
					Data_In => Shifter_Out_H5_1,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H5_2);
					
Shifter_H5_3: Shifter 
             port map(
					Data_In => Shifter_Out_H5_2,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H5_3);
					
Shifter_H5_4: Shifter 
             port map(
					Data_In => Shifter_Out_H5_3,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H5_4);
					
Shifter_H5_5: Shifter 
             port map(
					Data_In => Shifter_Out_H5_4,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H5_5);
					
Shifter_H5_6: Shifter 
             port map(
					Data_In => Shifter_Out_H5_5,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H5_6);
					
Shifter_H5_7: Shifter 
             port map(
					Data_In => Shifter_Out_H5_6,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H5_7);

Shifter_H6_0: Shifter 
             port map(
					Data_In => H_Filter_Out_6,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H6_0);

Shifter_H6_1: Shifter 
             port map(
					Data_In => Shifter_Out_H6_0,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H6_1);
					
Shifter_H6_2: Shifter 
             port map(
					Data_In => Shifter_Out_H6_1,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H6_2);
					
Shifter_H6_3: Shifter 
             port map(
					Data_In => Shifter_Out_H6_2,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H6_3);
					
Shifter_H6_4: Shifter 
             port map(
					Data_In => Shifter_Out_H6_3,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H6_4);
					
Shifter_H6_5: Shifter 
             port map(
					Data_In => Shifter_Out_H6_4,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H6_5);
					
Shifter_H6_6: Shifter 
             port map(
					Data_In => Shifter_Out_H6_5,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H6_6);
					
Shifter_H6_7: Shifter 
             port map(
					Data_In => Shifter_Out_H6_6,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H6_7);

Shifter_H7_0: Shifter 
             port map(
					Data_In => H_Filter_Out_7,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H7_0);

Shifter_H7_1: Shifter 
             port map(
					Data_In => Shifter_Out_H7_0,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H7_1);
					
Shifter_H7_2: Shifter 
             port map(
					Data_In => Shifter_Out_H7_1,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H7_2);
					
Shifter_H7_3: Shifter 
             port map(
					Data_In => Shifter_Out_H7_2,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H7_3);
					
Shifter_H7_4: Shifter 
             port map(
					Data_In => Shifter_Out_H7_3,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H7_4);
					
Shifter_H7_5: Shifter 
             port map(
					Data_In => Shifter_Out_H7_4,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H7_5);
					
Shifter_H7_6: Shifter 
             port map(
					Data_In => Shifter_Out_H7_5,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H7_6);
					
Shifter_H7_7: Shifter 
             port map(
					Data_In => Shifter_Out_H7_6,
					Clk => Clk, 
					Reset_n => Reset_n,
					Data_Out => Shifter_Out_H7_7);
					

V_Filter_0: Filter
				port map(
					Filter_In_0 => Shifter_Out_H0_7,
					Filter_In_1 => Shifter_Out_H0_6,
					Filter_In_2 => Shifter_Out_H0_5,
					Filter_In_3 => Shifter_Out_H0_4,
					Filter_In_4 => Shifter_Out_H0_3,
					Filter_In_5 => Shifter_Out_H0_2,
					Filter_In_6 => Shifter_Out_H0_1,
					Filter_In_7 => Shifter_Out_H0_0,
					Clk => Clk,
					Reset_n => Reset_n,
					Filter_En => Filter_V_Enable,
					Filter_Out => Interpolator_out_0);

V_Filter_1: Filter
				port map(
					Filter_In_0 => Shifter_Out_H1_7,
					Filter_In_1 => Shifter_Out_H1_6,
					Filter_In_2 => Shifter_Out_H1_5,
					Filter_In_3 => Shifter_Out_H1_4,
					Filter_In_4 => Shifter_Out_H1_3,
					Filter_In_5 => Shifter_Out_H1_2,
					Filter_In_6 => Shifter_Out_H1_1,
					Filter_In_7 => Shifter_Out_H1_0,
					Clk => Clk,
					Reset_n => Reset_n,
					Filter_En => Filter_V_Enable,
					Filter_Out => Interpolator_out_1);

V_Filter_2: Filter
				port map(
					Filter_In_0 => Shifter_Out_H2_7,
					Filter_In_1 => Shifter_Out_H2_6,
					Filter_In_2 => Shifter_Out_H2_5,
					Filter_In_3 => Shifter_Out_H2_4,
					Filter_In_4 => Shifter_Out_H2_3,
					Filter_In_5 => Shifter_Out_H2_2,
					Filter_In_6 => Shifter_Out_H2_1,
					Filter_In_7 => Shifter_Out_H2_0,
					Clk => Clk,
					Reset_n => Reset_n,
					Filter_En => Filter_V_Enable,
					Filter_Out => Interpolator_out_2);

V_Filter_3: Filter
				port map(
					Filter_In_0 => Shifter_Out_H3_7,
					Filter_In_1 => Shifter_Out_H3_6,
					Filter_In_2 => Shifter_Out_H3_5,
					Filter_In_3 => Shifter_Out_H3_4,
					Filter_In_4 => Shifter_Out_H3_3,
					Filter_In_5 => Shifter_Out_H3_2,
					Filter_In_6 => Shifter_Out_H3_1,
					Filter_In_7 => Shifter_Out_H3_0,
					Clk => Clk,
					Reset_n => Reset_n,
					Filter_En => Filter_V_Enable,
					Filter_Out => Interpolator_out_3);

V_Filter_4: Filter
				port map(
					Filter_In_0 => Shifter_Out_H4_7,
					Filter_In_1 => Shifter_Out_H4_6,
					Filter_In_2 => Shifter_Out_H4_5,
					Filter_In_3 => Shifter_Out_H4_4,
					Filter_In_4 => Shifter_Out_H4_3,
					Filter_In_5 => Shifter_Out_H4_2,
					Filter_In_6 => Shifter_Out_H4_1,
					Filter_In_7 => Shifter_Out_H4_0,
					Clk => Clk,
					Reset_n => Reset_n,
					Filter_En => Filter_V_Enable,
					Filter_Out => Interpolator_out_4);

V_Filter_5: Filter
				port map(
					Filter_In_0 => Shifter_Out_H5_7,
					Filter_In_1 => Shifter_Out_H5_6,
					Filter_In_2 => Shifter_Out_H5_5,
					Filter_In_3 => Shifter_Out_H5_4,
					Filter_In_4 => Shifter_Out_H5_3,
					Filter_In_5 => Shifter_Out_H5_2,
					Filter_In_6 => Shifter_Out_H5_1,
					Filter_In_7 => Shifter_Out_H5_0,
					Clk => Clk,
					Reset_n => Reset_n,
					Filter_En => Filter_V_Enable,
					Filter_Out => Interpolator_out_5);

V_Filter_6: Filter
				port map(
					Filter_In_0 => Shifter_Out_H6_7,
					Filter_In_1 => Shifter_Out_H6_6,
					Filter_In_2 => Shifter_Out_H6_5,
					Filter_In_3 => Shifter_Out_H6_4,
					Filter_In_4 => Shifter_Out_H6_3,
					Filter_In_5 => Shifter_Out_H6_2,
					Filter_In_6 => Shifter_Out_H6_1,
					Filter_In_7 => Shifter_Out_H6_0,
					Clk => Clk,
					Reset_n => Reset_n,
					Filter_En => Filter_V_Enable,
					Filter_Out => Interpolator_out_6);

V_Filter_7: Filter
				port map(
					Filter_In_0 => Shifter_Out_H7_7,
					Filter_In_1 => Shifter_Out_H7_6,
					Filter_In_2 => Shifter_Out_H7_5,
					Filter_In_3 => Shifter_Out_H7_4,
					Filter_In_4 => Shifter_Out_H7_3,
					Filter_In_5 => Shifter_Out_H7_2,
					Filter_In_6 => Shifter_Out_H7_1,
					Filter_In_7 => Shifter_Out_H7_0,
					Clk => Clk,
					Reset_n => Reset_n,
					Filter_En => Filter_V_Enable,
					Filter_Out => Interpolator_out_7);

end Behavioral;

