----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    19:14:04 12/23/2018 
-- Design Name: 
-- Module Name:    Sample_Three - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Sample_Three is
    Port ( Data_In : in  STD_LOGIC_VECTOR (7 downto 0);
           Sel : in  STD_LOGIC_VECTOR (3 downto 0);
           Data_Out : out STD_LOGIC_VECTOR (7 downto 0));
end Sample_Three;

architecture Behavioral of Sample_Three is

component Mux 
     Port ( A : in  STD_LOGIC_VECTOR (7 downto 0);
			  B : in  STD_LOGIC_VECTOR (7 downto 0);
			  Sel : in  STD_LOGIC;
           Data_Out : out  STD_LOGIC_VECTOR (7 downto 0));
 end component;

component Mux8x1
   Port ( S0 : in  STD_LOGIC_VECTOR (7 downto 0);
           S1 : in  STD_LOGIC_VECTOR (7 downto 0);
           S2 : in  STD_LOGIC_VECTOR (7 downto 0);
           S3 : in  STD_LOGIC_VECTOR (7 downto 0);
           S4 : in  STD_LOGIC_VECTOR (7 downto 0);
           S5 : in  STD_LOGIC_VECTOR (7 downto 0);
           S6 : in  STD_LOGIC_VECTOR (7 downto 0);
           S7 : in  STD_LOGIC_VECTOR (7 downto 0);
           Sel : in  STD_LOGIC_VECTOR (2 downto 0);
           Data_Out : out STD_LOGIC_VECTOR (7 downto 0));
end component;

component AddSub
 Port ( A : in  signed (7 downto 0);
        B : in  signed (7 downto 0);
        Add_Sub : in  STD_LOGIC;
        Data_Out : out  signed (7 downto 0));
end component;

signal InReg, OutReg : signed(7 downto 0) := (others => '0');
signal Add_Sub_Reg : signed(7 downto 0) := (others => '0');
signal S0_Reg, S1_Reg, S2_Reg, S3_Reg, S4_Reg, S5_Reg: signed(7 downto 0) := (others => '0');
signal A_Level_0_Reg, B_Level_0_Reg: signed(7 downto 0) := (others => '0');
signal A_Level_1_Reg, B_Level_1_Reg: signed(7 downto 0) := (others => '0');
signal Mux_2x1_Level_0_Reg, Mux_2x1_Level_1_Reg, Mux_8x1_Level_1_Reg  : std_logic_vector(7 downto 0);

begin

Mux_Level_0 : Mux 
					port map(
					 A =>  std_logic_vector(A_Level_0_Reg),
					 B =>  std_logic_vector(B_Level_0_Reg),
					 Sel =>  Sel(3),
					 Data_Out => Mux_2x1_Level_0_Reg
					);
					
Mux_Level_1 : Mux 
					port map(
					 A =>  std_logic_vector(A_Level_1_Reg),
					 B =>  std_logic_vector(B_Level_1_Reg),
					 Sel =>  Sel(0),
					 Data_Out => Mux_2x1_Level_1_Reg
					);
					
Mux_8x1_Level_1 : Mux8x1 
               port map(
					S0 => std_logic_vector(S0_Reg),
					S1 => std_logic_vector(S1_Reg),
					S2 => std_logic_vector(S2_Reg),
					S3 => std_logic_vector(S3_Reg),
					S4 => std_logic_vector(S4_Reg),
					S5 => std_logic_vector(S5_Reg),
					S6 => (others => '-'),
					S7 => (others => '-'),
					Sel => Sel(3 downto 1),
					Data_Out => Mux_8x1_Level_1_Reg
					);
					
Add_Sub_Level_0 : AddSub 
                   port map(
						 A => signed(Mux_2x1_Level_1_Reg),
						 B => signed(Mux_8x1_Level_1_Reg),
						 Add_Sub => Sel(3),
						 Data_Out => Add_Sub_Reg
						 );			
						 
InReg <= signed(Data_In);
Data_Out <= std_logic_vector(OutReg);
A_Level_0_Reg <= shift_left(InReg, 7);
B_Level_0_Reg <= shift_left(InReg, 6);
A_Level_1_Reg <= InReg + signed(Mux_2x1_Level_0_Reg);
B_Level_1_Reg <= InReg;

S0_Reg <= shift_left(InReg, 4);
S1_Reg <= shift_left(InReg,5);
S2_Reg <= shift_left(InReg,2);
S3_Reg <= shift_left(InReg,3);
S4_Reg <= shift_left(A_Level_1_Reg,2);
S5_Reg <= shift_left(A_Level_1_Reg,3);

  process (Sel, InReg)
  begin
  case Sel is
				when "0000" => 
                OutReg <= S1_Reg;  -- 
            when "0001" => 
                OutReg <= S0_Reg; 
            when "0010" => 
                OutReg <= (others => '0');  
            when "0011" => 
                OutReg <= Add_Sub_Reg;
            when "0100" => 
                OutReg <= S4_Reg;   
            when "0101" => 
                OutReg <= S5_Reg; 
            when "0110" => 
                OutReg <= A_Level_1_Reg; 					 
            when others =>
                OutReg <= (others => 'Z');
  end case;
end process;


end Behavioral;

