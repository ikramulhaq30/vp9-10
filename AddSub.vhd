----------------------------------------------------------------------------------

--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity AddSub is
    Port ( A : in  signed (7 downto 0);
           B : in  signed (7 downto 0);
           Add_Sub : in  STD_LOGIC;
           Data_Out : out  signed (7 downto 0));
end AddSub;

architecture Behavioral of AddSub is

begin

process (A, B, Add_Sub)
begin
   case Add_Sub is
	 when '1' => 
	   Data_Out <= A - B;		
	when '0' =>
	  Data_Out <= A + B;
	 when others => 
	  Data_Out <= (others => 'Z');
	end case;
 
end process;

end Behavioral;

