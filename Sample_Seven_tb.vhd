--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   00:30:11 01/06/2019
-- Design Name:   
-- Module Name:   F:/Master/Vp9-10/Vp9-10/Sample_Seven_tb.vhd
-- Project Name:  Vp9-10
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: Sample_Seven
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY Sample_Seven_tb IS
END Sample_Seven_tb;
 
ARCHITECTURE behavior OF Sample_Seven_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT Sample_Seven
    PORT(
         Data_In : IN  std_logic_vector(7 downto 0);
         Sel : IN  std_logic_vector(2 downto 0);
         Data_Out : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal Data_In : std_logic_vector(7 downto 0) := (others => '0');
   signal Sel : std_logic_vector(2 downto 0) := (others => '0');

 	--Outputs
   signal Data_Out : std_logic_vector(7 downto 0);
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: Sample_Seven PORT MAP (
          Data_In => Data_In,
          Sel => Sel,
          Data_Out => Data_Out
        );

    -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 50 ns.
      wait for 50 ns;	
		Data_In <= "10101011";
		Sel <= "000";
		wait for 50 ns;
		Sel <= "001";
		wait for 40 ns;
		Sel <= "010";
		wait for 50 ns;
		Sel <= "011";
		wait for 50 ns;
		Sel <= "100";
		wait for 50 ns;
		Sel <= "101";
		wait for 50 ns;
		Sel <= "110";
		wait for 50 ns;
		Sel <= "111";
      wait;
   end process;

END;
