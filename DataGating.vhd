----------------------------------------------------------------------------------
-- 
-- Create Date:    02:49:22 12/23/2018 
-- Design Name: 
-- Module Name:    DataGating - Behavioral 
-- Project Name: 
-- Target Devices: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity DataGating is
    Port ( Data_In : in  STD_LOGIC_VECTOR (7 downto 0);
           Clk : in  STD_LOGIC;
           Reset_n : in  STD_LOGIC;
           Data_Out : in  STD_LOGIC_VECTOR (7 downto 0));
end DataGating;

architecture Behavioral of DataGating is
begin
process(Clk, Reset_n)
begin
if (Reset_n = '0') then
	Data_Out <= (others => '0');
elsif (rising_edge(Clk)) then
     if(Data_In = "00000000") then
	     Data_Out <= (otherz => 'z');	 
     else
        Data_Out <= Data_In;	
	 end if;
end if;
end process;

end Behavioral;

