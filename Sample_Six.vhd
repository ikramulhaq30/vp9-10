----------------------------------------------------------------------------------
-- Module Name:    Sample_Six - Behavioral 
-- Project Name: 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Sample_Six is
    Port ( Data_In : in  STD_LOGIC_VECTOR (7 downto 0);
           Sel : in  STD_LOGIC_VECTOR (3 downto 0);
           Data_Out : out  STD_LOGIC_VECTOR (7 downto 0));
end Sample_Six;

architecture Behavioral of Sample_Six is
component Mux 
     Port ( A : in  STD_LOGIC_VECTOR (7 downto 0);
			  B : in  STD_LOGIC_VECTOR (7 downto 0);
			  Sel : in  STD_LOGIC;
           Data_Out : out  STD_LOGIC_VECTOR (7 downto 0));
end component;

component Mux8x1 
  Port  (  S0 : in  STD_LOGIC_VECTOR (7 downto 0);
           S1 : in  STD_LOGIC_VECTOR (7 downto 0);
           S2 : in  STD_LOGIC_VECTOR (7 downto 0);
           S3 : in  STD_LOGIC_VECTOR (7 downto 0);
			  S4 : in  STD_LOGIC_VECTOR (7 downto 0);
			  S5 : in  STD_LOGIC_VECTOR (7 downto 0);
			  S6 : in  STD_LOGIC_VECTOR (7 downto 0);
			  S7 : in  STD_LOGIC_VECTOR (7 downto 0);
           Sel : in  STD_LOGIC_VECTOR (2 downto 0);
           Data_Out : out  STD_LOGIC_VECTOR (7 downto 0));
end component;

component AddSub
 Port ( A : in  signed (7 downto 0);
        B : in  signed (7 downto 0);
        Add_Sub : in  STD_LOGIC;
        Data_Out : out  signed (7 downto 0));
end component;

signal InReg, OutReg : signed(7 downto 0) := (others => '0');
signal Add_Sub_Reg_Level_0 : signed(7 downto 0) := (others => '0');

signal S0_Level_1_Reg, S1_Level_1_Reg, S2_Level_1_Reg, S3_Level_1_Reg, S4_Level_1_Reg: signed(7 downto 0) := (others => '0');
signal A_Level_0_Reg, B_Level_0_Reg: signed(7 downto 0) := (others => '0');
signal A_Level_1_Reg, B_Level_1_Reg: signed(7 downto 0) := (others => '0');
signal Add_Sub_Level_0_Reg: signed(7 downto 0) := (others => '0');
signal Mux_2x1_Level_0_Reg, Mux_8x1_Level_1_Reg, Mux_2x1_Level_1_Reg: std_logic_vector(7 downto 0);

begin				
	Mux_Level_0 : Mux 
					port map(
					 A =>  std_logic_vector(A_Level_0_Reg),
					 B =>  std_logic_vector(B_Level_0_Reg),
					 Sel =>  Sel(3),
					 Data_Out => Mux_2x1_Level_0_Reg
					);
					
	Mux_8x1_Level_1 : Mux8x1 
               port map(
					S0 => std_logic_vector(S0_Level_1_Reg),
					S1 => std_logic_vector(S1_Level_1_Reg),
					S2 => std_logic_vector(S2_Level_1_Reg),
					S3 => std_logic_vector(S3_Level_1_Reg),
					S4 => std_logic_vector(S4_Level_1_Reg),
					S5 => (others => '-'),
					S6 => (others => '-'),
					S7 => (others => '-'),
					Sel => Sel(3 downto 1),
					Data_Out => Mux_8x1_Level_1_Reg
					);
									
	Mux_Level_1 : Mux 
					port map(
					 A =>  std_logic_vector(A_Level_1_Reg),
					 B =>  std_logic_vector(B_Level_1_Reg),
					 Sel =>  Sel(0),
					 Data_Out => Mux_2x1_Level_1_Reg
					);
						
	Add_Sub_Level_0 : AddSub 
                   port map(
						 A => signed(Mux_8x1_Level_1_Reg),
						 B => signed(Mux_2x1_Level_1_Reg),
						 Add_Sub => Sel(3),
						 Data_Out => Add_Sub_Reg_Level_0
						 );
						 
	InReg <= signed(Data_In);
	Data_Out <= std_logic_vector(OutReg);
	
	A_Level_0_Reg <= shift_left(InReg, 1);
	B_Level_0_Reg <= shift_left(InReg, 2);
	
	S0_Level_1_Reg <= shift_left(InReg, 1);
	S1_Level_1_Reg <= shift_left(InReg, 2);
	S2_Level_1_Reg <= shift_left(InReg, 3);
	S3_Level_1_Reg <= shift_left(InReg, 4);
	S4_Level_1_Reg <= signed(Mux_2x1_Level_0_Reg) + InReg;
	
	A_Level_1_Reg <= InReg;
	B_Level_1_Reg <= signed(Mux_2x1_Level_0_Reg) + InReg;
	
process (Sel, InReg)
  begin
  case Sel is
				when "0000" => 
                OutReg <= shift_left(InReg, 1);  
            when "0001" => 
                OutReg <= shift_left(InReg, 2); 
            when "0010" => 
                OutReg <= shift_left(InReg, 4);  
            when "0011" => 
                OutReg <= (others => '0');
            when "0100" => 
                OutReg <= Add_Sub_Reg_Level_0;   
            when "0101" => 
                OutReg <= shift_left(Add_Sub_Reg_Level_0, 2); 
            when "0110" => 
                OutReg <= shift_left(Add_Sub_Reg_Level_0, 4); 	
				when others =>
                OutReg <= (others => 'Z');
  end case;
end process;
end Behavioral;

