----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    02:39:43 12/23/2018 
-- Design Name: 
-- Module Name:    Shifter - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Shifter is
    Port ( Data_In : in  STD_LOGIC_VECTOR (7 downto 0);
           Clk : in  STD_LOGIC;
           Reset_n : in  STD_LOGIC;
           Data_Out : out  STD_LOGIC_VECTOR (7 downto 0));
end Shifter;

architecture Behavioral of Shifter is
begin
process (Clk, Reset_n)
begin
 if(Reset_n = '0') then
   Data_Out <= (others => '0');
	elsif (rising_edge(Clk)) then
			Data_Out <= Data_In;
	end if;
end process;
end Behavioral;

